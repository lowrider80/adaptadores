package com.lowrider.adapters_lowrider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lowrider.adapters_lowrider.adapters.adaptermiwito;
import com.lowrider.adapters_lowrider.models.ProductoMiwito;

import java.util.ArrayList;
import java.util.List;

public class EjercicioMiwito extends AppCompatActivity {

    private List<ProductoMiwito>productoMiwitoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio_miwito);
        RecyclerView recyclerMiwito=findViewById(R.id.recyclerMiwito);


        populateMiwito();

        recyclerMiwito.setAdapter(new adaptermiwito(R.layout.item_miwito,productoMiwitoList,this));

        final LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerMiwito.setLayoutManager(linearLayoutManager);



    }

    private void populateMiwito() {
        productoMiwitoList=new ArrayList<>();
        for(int i=0;i<5;i++){
            ProductoMiwito productoMiwito= new ProductoMiwito();
            productoMiwito.setId(i);
            productoMiwito.setTitulo("Producto "+i);
            productoMiwito.setDescripcion("Descripcion "+i);
            productoMiwito.setObservacion("Observacion"+i);
            productoMiwitoList.add(productoMiwito);
        }
    }


}
