package com.lowrider.adapters_lowrider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button1=findViewById(R.id.button1);
        Button button2 =findViewById(R.id.button2);
        Button button3=findViewById(R.id.button3);
        Button button4=findViewById(R.id.button4);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1: {
                Intent i = new Intent(MainActivity.this, Basic3Activity.class);
                startActivity(i);
                break;
            }
            case R.id.button2: {
                Intent i = new Intent(MainActivity.this, Basic4Activity.class);
                startActivity(i);
                break;
            }
            case R.id.button3: {
                Intent i = new Intent(MainActivity.this, Adapter3Activity.class);
                startActivity(i);
                break;
            }
            case R.id.button4: {
                Intent i = new Intent(MainActivity.this, EjercicioMiwito.class);
                startActivity(i);
                break;
            }

            default: break;
        }
    }
}
