package com.lowrider.adapters_lowrider.interfaces;

import android.view.View;
import android.widget.ImageView;

public interface OnItemClickListener {

    void onClikListener(int position, View container, ImageView imageView);

    void onButtonClickListener(String cadena);

}
