package com.lowrider.adapters_lowrider.interfaces;

import android.view.View;
import android.widget.ImageView;

public interface OnCheckedChangeListener {


     void onCheckListener(int position, View container, ImageView imageView);
}
