package com.lowrider.adapters_lowrider;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.lowrider.adapters_lowrider.adapters.adapterBasic4;
import com.lowrider.adapters_lowrider.api.apiRetrofit;
import com.lowrider.adapters_lowrider.api.getRetrofit;
import com.lowrider.adapters_lowrider.interfaces.OnItemClickListener;
import com.lowrider.adapters_lowrider.models.Pokemon;
import com.lowrider.adapters_lowrider.models.response.PokemonResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Basic4Activity extends AppCompatActivity implements OnItemClickListener {

    private RecyclerView recyclerView;
    private PokemonResponse pokemonResponse;
    private ArrayList<Pokemon> pokemonArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic4);

        bindUI();
        getDatos();
    }


    private void bindUI() {
        recyclerView=findViewById(R.id.recyclerView);

    }


    private void getDatos() {

        getRetrofit getRetrofit= apiRetrofit.getConfiguration().create(getRetrofit.class);
        Call<PokemonResponse> pokemonResponseCall=getRetrofit.getListPokemon(20,0);
        pokemonResponseCall.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(Call<PokemonResponse> call, Response<PokemonResponse> response) {

                pokemonResponse=response.body();
                pokemonArrayList=pokemonResponse.getResults();
                implementarAdapter(pokemonArrayList);

            }

            @Override
            public void onFailure(Call<PokemonResponse> call, Throwable t) {
                Toast.makeText(Basic4Activity.this,"Hubo un problema", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void implementarAdapter(ArrayList<Pokemon> pokemonResponse) {
        recyclerView.setAdapter(new adapterBasic4(this,R.layout.item_pokedex,
                pokemonResponse,this));

        final LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


    }



    @Override
    public void onClikListener(int position, View container, ImageView imageView) {

        if(pokemonArrayList!=null){
            Pokemon pokemon= pokemonArrayList.get(position);
            gotoDetailsAnimation(pokemon,imageView);
        }

    }

    @Override
    public void onButtonClickListener(String cadena) {

    }


    private void gotoDetailsAnimation(Pokemon pokemon,ImageView imageView){
        Intent intent= new Intent(this,Basic4DetailActivity.class);
        intent.putExtra("POKEMON", pokemon);
        intent.putExtra("IMAGE_TRANSITION", ViewCompat.getTransitionName(imageView));

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                imageView,
                ViewCompat.getTransitionName(imageView));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent,options.toBundle());
        }
    }
}
