package com.lowrider.adapters_lowrider.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lowrider.adapters_lowrider.R;
import com.lowrider.adapters_lowrider.interfaces.OnItemClickListener;
import com.lowrider.adapters_lowrider.models.Pokemon;


import java.util.List;

public class adapterBasic4 extends RecyclerView.Adapter<adapterBasic4.viewHolder>{


    private int layout;
    Context context;
    List<Pokemon> pokemonArrayList;
    private final OnItemClickListener onItemClickListener;

    public adapterBasic4(Context context,int layout,List<Pokemon>pokemonArrayList,OnItemClickListener onItemClickListener) {
        this.context=context;
        this.layout=layout;
        this.pokemonArrayList=pokemonArrayList;
        this.onItemClickListener = onItemClickListener;
    }



    @NonNull
    @Override
    public adapterBasic4.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);
        context = parent.getContext();
        return new adapterBasic4.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final adapterBasic4.viewHolder holder, final int position) {

        holder.bind(pokemonArrayList.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener!=null){
                    Log.v("ADAPTER", "iviPhoto "+holder.imageViewPokemon);
                    ViewCompat.setTransitionName(holder.imageViewPokemon, "iviPhoto");
                    onItemClickListener.onClikListener(position,holder.itemView,holder.imageViewPokemon);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return pokemonArrayList.size();
    }



    public class viewHolder extends RecyclerView.ViewHolder{

        ImageView imageViewPokemon;
        TextView textViewName;
        TextView  textViewSubName;
        RelativeLayout relativeLayoutPokemon;

        public viewHolder(View itemView) {
            super(itemView);
            imageViewPokemon=itemView.findViewById(R.id.imageViewPokemon);
            textViewName=itemView.findViewById(R.id.textViewName);
            textViewSubName=itemView.findViewById(R.id.textViewSubName);
            relativeLayoutPokemon=itemView.findViewById(R.id.relativeLayoutPokemon);
        }

        public void bind(Pokemon pokemon){
            textViewName.setText(pokemon.getName());
            textViewSubName.setText(String.valueOf(pokemon.getNumber()));
            Glide.with(context).load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pokemon.getNumber()
                    +".png").centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewPokemon);





        }


    }

}
