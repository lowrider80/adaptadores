package com.lowrider.adapters_lowrider.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lowrider.adapters_lowrider.R;
import com.lowrider.adapters_lowrider.models.ProductoMiwito;

import java.util.List;

/**
 * Created by AcademiaMoviles on 07/07/2018.
 */

public class adaptermiwito extends RecyclerView.Adapter<adaptermiwito.ViewHolder>{

    int layout;
    List<ProductoMiwito> productoMiwitoList;
    Context context;

    public adaptermiwito(int layout, List<ProductoMiwito> productoMiwitoList, Context context) {
        this.layout = layout;
        this.productoMiwitoList = productoMiwitoList;
        this.context = context;
    }

    @NonNull
    @Override
    public adaptermiwito.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);
        context = parent.getContext();
        return  new ViewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull adaptermiwito.ViewHolder holder, int position) {
        holder.bind(productoMiwitoList.get(position));

    }

    @Override
    public int getItemCount() {
        return productoMiwitoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView titulo;
        private TextView descripcion;


        public ViewHolder(View itemView) {
            super(itemView);
            titulo=itemView.findViewById(R.id.titulo);
            descripcion=itemView.findViewById(R.id.descripcion);
        }

        public void bind(ProductoMiwito productoMiwito){

            titulo.setText(productoMiwito.getTitulo());
            descripcion.setText(productoMiwito.getObservacion());

        }
    }

}
