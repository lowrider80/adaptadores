package com.lowrider.adapters_lowrider.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.lowrider.adapters_lowrider.R;
import com.lowrider.adapters_lowrider.interfaces.OnCheckedChangeListener;
import com.lowrider.adapters_lowrider.interfaces.OnItemClickListener;
import com.lowrider.adapters_lowrider.models.DistritoTipo;


import java.util.List;

public class adapterDistrito extends RecyclerView.Adapter<adapterDistrito.viewHolder> {

    private int layout;
    Context context;
    private List<DistritoTipo> distritoTipoList;
    OnItemClickListener onItemClickListener;


    public adapterDistrito(int layout, Context context, List<DistritoTipo> distritoTipoList,OnItemClickListener onItemClickListener) {
        this.layout = layout;
        this.context = context;
        this.distritoTipoList = distritoTipoList;
        this.onItemClickListener=onItemClickListener;
    }


    @NonNull
    @Override
    public adapterDistrito.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(layout,parent,false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterDistrito.viewHolder holder, int position) {
        holder.bind(distritoTipoList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return distritoTipoList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        private TextView textView;
        private CheckBox checkBox;
        private Button btnDone;

        public viewHolder(View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.textView);
            checkBox=itemView.findViewById(R.id.checkBox);
            btnDone=(Button)itemView.findViewById(R.id.btnDone);
        }

        public void bind(DistritoTipo distritoTipo, final int position){

            textView.setText(distritoTipo.getItemName());
            checkBox.setChecked(distritoTipo.isState());
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(checkBox.isChecked()){
                        distritoTipoList.get(position).setState(true);
                    }else{
                        distritoTipoList.get(position).setState(false);
                    }

                }
            });


            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickListener!=null){
                        onItemClickListener.onButtonClickListener(reviewCheck(distritoTipoList));
                    }
                }
            });
        }

    }

    public String reviewCheck(List<DistritoTipo> distritoTipoList){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<distritoTipoList.size();i++){
            if(distritoTipoList.get(i).isState()){
                sb.append(distritoTipoList.get(i).getItemName());
            }
        }
        String msg=sb.toString();
        return msg;

    }



}
