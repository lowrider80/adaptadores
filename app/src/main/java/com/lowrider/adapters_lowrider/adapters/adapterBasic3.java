package com.lowrider.adapters_lowrider.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lowrider.adapters_lowrider.R;
import com.lowrider.adapters_lowrider.models.Pokemon;
import java.util.List;

public class adapterBasic3 extends RecyclerView.Adapter<adapterBasic3.viewHolder> {


    private int layout;
    Context context;
    List<Pokemon> pokemonArrayList;

    public adapterBasic3(Context context,int layout,List<Pokemon>pokemonArrayList) {
    this.context=context;
    this.layout=layout;
    this.pokemonArrayList=pokemonArrayList;
    }



    @NonNull
    @Override
    public adapterBasic3.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);
        context = parent.getContext();
        return  new viewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull adapterBasic3.viewHolder holder, int position) {

        holder.bind(pokemonArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return pokemonArrayList.size();
    }



    public class viewHolder extends RecyclerView.ViewHolder{

        ImageView imageViewPokemon;
        TextView textViewName;
        TextView  textViewSubName;

        public viewHolder(View itemView) {
            super(itemView);
            imageViewPokemon=itemView.findViewById(R.id.imageViewPokemon);
            textViewName=itemView.findViewById(R.id.textViewName);
            textViewSubName=itemView.findViewById(R.id.textViewSubName);
        }

        public void bind(Pokemon pokemon){
            textViewName.setText(pokemon.getName());
            textViewSubName.setText(String.valueOf(pokemon.getNumber()));
            Glide.with(context).load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pokemon.getNumber()
                    +".png").centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewPokemon);
        }


    }

}
