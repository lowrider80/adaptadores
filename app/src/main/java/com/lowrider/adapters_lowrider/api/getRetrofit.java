package com.lowrider.adapters_lowrider.api;

import com.lowrider.adapters_lowrider.models.response.PokemonResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface getRetrofit {


    @GET("pokemon")
    Call<PokemonResponse> getListPokemon(@Query("limit")int limit, @Query("offset")int offset);






}
