package com.lowrider.adapters_lowrider.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class apiRetrofit {


    public static Retrofit getConfiguration(){
        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


}
