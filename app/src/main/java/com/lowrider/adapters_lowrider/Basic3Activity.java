package com.lowrider.adapters_lowrider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.lowrider.adapters_lowrider.adapters.adapterBasic3;
import com.lowrider.adapters_lowrider.api.apiRetrofit;
import com.lowrider.adapters_lowrider.api.getRetrofit;
import com.lowrider.adapters_lowrider.models.Pokemon;
import com.lowrider.adapters_lowrider.models.response.PokemonResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Basic3Activity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private PokemonResponse pokemonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic3);
        bindUI();
        getDatos();


    }


    private void bindUI() {
       recyclerView=findViewById(R.id.recyclerView);

    }


    private void getDatos() {

        getRetrofit getRetrofit= apiRetrofit.getConfiguration().create(getRetrofit.class);
        Call<PokemonResponse>pokemonResponseCall=getRetrofit.getListPokemon(150,0);
        pokemonResponseCall.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(Call<PokemonResponse> call, Response<PokemonResponse> response) {

                pokemonResponse=response.body();
                ArrayList<Pokemon> pokemonArrayList=pokemonResponse.getResults();
                implementarAdapter(pokemonArrayList);

            }

            @Override
            public void onFailure(Call<PokemonResponse> call, Throwable t) {
                Toast.makeText(Basic3Activity.this,"Hubo un problema", Toast.LENGTH_LONG).show();

            }
        });


    }

    private void implementarAdapter(ArrayList<Pokemon> pokemonResponse) {
        recyclerView.setAdapter(new adapterBasic3(this,R.layout.item_pokedex,
                pokemonResponse));

        final LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


    }


}
