package com.lowrider.adapters_lowrider.models;

/**
 * Created by AcademiaMoviles on 07/07/2018.
 */

public class ProductoMiwito {

    private int id;
    private String titulo;
    private String descripcion;
    private String observacion;

    public ProductoMiwito() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
