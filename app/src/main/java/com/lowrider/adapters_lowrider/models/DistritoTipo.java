package com.lowrider.adapters_lowrider.models;

public class DistritoTipo {

    private int id;
    private String itemName;
    private boolean state;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DistritoTipo() {
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
