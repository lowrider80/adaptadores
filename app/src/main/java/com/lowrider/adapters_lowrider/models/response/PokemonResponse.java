package com.lowrider.adapters_lowrider.models.response;

import com.lowrider.adapters_lowrider.models.Pokemon;

import java.util.ArrayList;


public class PokemonResponse {


    ArrayList<Pokemon> results;



    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResult(ArrayList<Pokemon> results) {
        this.results = results;
    }



}
