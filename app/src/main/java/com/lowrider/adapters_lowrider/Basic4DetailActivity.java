package com.lowrider.adapters_lowrider;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lowrider.adapters_lowrider.models.Pokemon;

public class Basic4DetailActivity extends AppCompatActivity {

    private Pokemon pokemon;
    private String imageTransitionName;

    private ImageView imageViewPokemon;
    private TextView textViewName;
    private TextView textViewSubName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic4_detail);
        bindUI();
        extras();
        populate();


    }

    private void populate() {
        if(pokemon!=null){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageViewPokemon.setTransitionName(imageTransitionName);
            }

            Glide.with(this).load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pokemon.getNumber()
                    +".png").centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewPokemon);

            textViewName.setText(pokemon.getName());
            textViewSubName.setText(String.valueOf(pokemon.getNumber()));
        }


    }

    private void extras() {
        if(getIntent()!=null && getIntent().getExtras()!=null){
            pokemon= (Pokemon) getIntent().getExtras().getSerializable("POKEMON");
            imageTransitionName= getIntent().getExtras().getString("IMAGE_TRANSITION");
        }
    }

    private void bindUI() {
        imageViewPokemon= (ImageView) findViewById(R.id.imageViewPokemon);
        textViewName= (TextView)findViewById(R.id.textViewName);
        textViewSubName= (TextView)findViewById(R.id.textViewSubName);
    }
}
