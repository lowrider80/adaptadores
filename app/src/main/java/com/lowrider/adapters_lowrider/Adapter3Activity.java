package com.lowrider.adapters_lowrider;

;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.lowrider.adapters_lowrider.adapters.adapterDistrito;
import com.lowrider.adapters_lowrider.interfaces.OnItemClickListener;
import com.lowrider.adapters_lowrider.models.DistritoTipo;
import java.util.ArrayList;
import java.util.List;

public class Adapter3Activity extends AppCompatActivity implements View.OnClickListener,OnItemClickListener {

    private ImageView imageViewDistrito;
    private ImageView imageViewTipo;
    private List<DistritoTipo> distritoTipoList;
    private List<DistritoTipo> distritoTipoList2;
    private TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter3);

        bindUI();
        listpopulate();


        imageViewDistrito.setOnClickListener(this);
        imageViewTipo.setOnClickListener(this);


    }

    private void listpopulate() {
        distritoTipoList= new ArrayList<>();

        for(int i=0;i<10;i++){
            DistritoTipo distritoTipo=new DistritoTipo();
            distritoTipo.setId(i);
            distritoTipo.setItemName("Item "+(i+1));
            if(i%2==0)distritoTipo.setState(true);
            else distritoTipo.setState(false);
            distritoTipoList.add(distritoTipo);
        }

        distritoTipoList2= new ArrayList<>();
        for(int i=0;i<10;i++){
            DistritoTipo distritoTipo=new DistritoTipo();
            distritoTipo.setId(i);
            distritoTipo.setItemName("Item "+(i+1));
            if(i%3==0)distritoTipo.setState(true);
            else distritoTipo.setState(false);
            distritoTipoList2.add(distritoTipo);
        }

    }

    private void bindUI() {

         imageViewDistrito=findViewById(R.id.imageViewDistrito);
         imageViewTipo=findViewById(R.id.imageViewTipo);
         textView2=findViewById(R.id.textView2);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imageViewDistrito:{
                populateRecyclerView(distritoTipoList);
                break;
            }
            case R.id.imageViewTipo:{
                populateRecyclerView(distritoTipoList2);
                break;
            }
            default: break;
        }
    }


    public void populateRecyclerView(List<DistritoTipo>distritoTipoList){
        MaterialDialog dialog =
                new MaterialDialog.Builder(this)
                        .customView(R.layout.dialog_distrito, true)
                        .cancelable(true)
                        .backgroundColorRes(R.color.colorPrimary)
                        .build();
        RecyclerView recyclerViewDistrito= (RecyclerView) dialog.findViewById(R.id.recyclerViewDistrito);
        Button btnDone=(Button)dialog.findViewById(R.id.btnDone);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerViewDistrito.setAdapter(new adapterDistrito(R.layout.item_distrito,this,distritoTipoList,this));
        recyclerViewDistrito.setLayoutManager(mLayoutManager);
        recyclerViewDistrito.setHasFixedSize(true);
        recyclerViewDistrito.setItemAnimator(new DefaultItemAnimator());
        dialog.show();

    }


    @Override
    public void onClikListener(int position, View container, ImageView imageView) {

    }

    @Override
    public void onButtonClickListener(String cadena) {

        if(distritoTipoList!=null){
            textView2.setText(cadena);
        }

    }
}
